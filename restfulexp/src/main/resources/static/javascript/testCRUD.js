$( document ).ready(function() {
    console.log( "ready!" );
    //alert("readyyyy");
    
    $('#element_7').on('change', function() {
    	  $( "li,#li_btn" ).not( "#li_7,#li_btn" ).addClass( "hidden" );
    	  console.log( this.value,"valorChange" );
    	  
    	  switch (this.value) {
    	  case '1'://Persona_Crear
    		  $( "#li_1,#li_4,#li_5,#li_3,#li_btn" ).removeClass( "hidden" );
    	    break;
    	  case '2'://Persona_Consultar
    		  $( "#li_1,#li_btn" ).removeClass( "hidden" );
    	    break;
    	  case '3'://Persona_Modificar
    		  $( "#li_1,#li_4,#li_btn" ).removeClass( "hidden" );
    	    break;
    	  case '4'://Persona_Eliminar
    		  $( "#li_1,#li_btn" ).removeClass( "hidden" );
    	    break;
    	  case '5'://Operacion_Crear
    		  $( "#li_1,#li_5,#li_3,#li_btn" ).removeClass( "hidden" );
    	    break;
    	  case '6'://Operacion_Consultar
    	    break;
    	  case '7'://Operacion_Modificar
    		  $( "#li_1,#li_2,#li_5,#li_3,#li_btn" ).removeClass( "hidden" );
    	    break;
    	  case '8'://Operacion_Eliminar
    		  $( "#li_1,#li_2,#li_btn" ).removeClass( "hidden" );
    	    break; 
    	  case '0'://Consultar todo
    		  $( "#li_btn" ).removeClass( "hidden" );
      	    break;
    	  default: //sin selección
    		  alert( "default" );
    		  $( "li,#li_btn" ).not( "#li_7" ).addClass( "hidden" );
    	  }
    	  
    	});
    
    
    
    $("#saveForm").click(function(){
	  console.log($('#element_7').val(),"enviado")
	  
	  switch ($('#element_7').val()) {
    	  case '1'://Persona_Crear
    		  var bolNext=validarBlancos($( "#li_1,#li_4,#li_5,#li_3" ));
    		  if(bolNext){
    			  var params={idPerson:$('#element_1').val(), name:$('#element_4_1').val() + $('#element_4_2').val(), value:$('#element_3_1').val(), card:$('#element_5').val()};
    			  console.log(params);
    			  request("POST","http://localhost:8080/CreatePerson",params)
    		  }
    	    break;
    	  case '2'://Persona_Consultar
    		  var bolNext=validarBlancos($( "#li_1" ));
    		  if(bolNext){
    			  var params={idPerson:$('#element_1').val()};
    			  console.log(params,"params");
    			  request("GET","http://localhost:8080/person",params)
    		  }
    	    break;
    	  case '3'://Persona_Modificar
    		  var bolNext=validarBlancos($( "#li_1,#li_4" ));
    		  if(bolNext){
    			  var params={idPerson:$('#element_1').val(), name:$('#element_4_1').val() + $('#element_4_2').val()};
    			  console.log(params);
    			  request("POST","http://localhost:8080/UpdatePerson",params)
    		  }
    	    break;
    	  case '4'://Persona_Eliminar
    		  var bolNext=validarBlancos($( "#li_1" ));
    		  if(bolNext){
    			  var params={idPerson:$('#element_1').val()};
    			  console.log(params);
    			  request("POST","http://localhost:8080/DeletePerson",params)
    		  }
    	    break;
    	  case '5'://Operacion_Crear
    		  var bolNext=validarBlancos($( "#li_1,#li_5,#li_3" ));
    		  if(bolNext){
    			  var params={idPerson:$('#element_1').val(), value:$('#element_3_1').val(), card:$('#element_5').val()};
    			  console.log(params);
    			  request("POST","http://localhost:8080/CreateOperation",params)
    		  }
    	    break;
    	  case '6'://Operacion_Consultar
    	    break;
    	  case '7'://Operacion_Modificar
    		  var bolNext=validarBlancos($( "#li_1,#li_2,#li_5,#li_3" ));
    		  if(bolNext){
    			  var params={idPerson:$('#element_1').val(), IdOp:$('#element_2').val(), value:$('#element_3_1').val(), card:$('#element_5').val()};
    			  console.log(params);
    			  request("POST","http://localhost:8080/UpdateOperation",params)
    		  }
    	    break;
    	  case '8'://Operacion_Eliminar
    		  var bolNext=validarBlancos($( "#li_1,#li_2" ));
    		  if(bolNext){
    			  var params={idPerson:$('#element_1').val(), IdOp:$('#element_2').val()};
    			  console.log(params);
    			  request("POST","http://localhost:8080/DeleteOperation",params)
    		  }
    	    break; 
    	  case '0'://Consultar todo
    		  var params={};
			  console.log(params);
			  request("GET","http://localhost:8080/people",params)
    	  }
	  
	});
    
    
});

function validarBlancos(elementos){
	var bolHigh,bolSend;
	bolSend=true;
	elementos.each(function( index ) {
		$( this ).find('[id^=element_]').each(function( index, value ) {
			console.log(value.value,"elemnto valor");
			if(!value.value)
				bolHigh=true;
			else if(value.id=="element_3_1" && isNaN(value.value))
				bolHigh=true;
				
		});
		if(bolHigh){
			bolSend=false;
			$( this ).addClass( "highlighted" );
		}
		bolHigh=false;
		//console.log($( this ).find('[id^=element_]'),"li en vali");
		//$( this ).addClass( "highlighted" );
		});
	return bolSend;
}

function request(type,url,params){
	jQuery.ajax({
        type:type,
        dataType:"json",
        url: url,
        data: params,
        success: function(data) {
        	console.log(data);
        	$('#element_6').val(JSON.stringify(data,null, 4));
        	$( "#element_6" ).attr("rows", "15");
        	$( "#li_6" ).removeClass( "hidden" );
            //successmessage = 'Data was succesfully captured';
            //$("label#successmessage").text(successmessage);
        },
        error: function(data) {
            successmessage = 'Error';
            $("label#successmessage").text(successmessage);
        },
    });
}

