package com.example.demo;

import java.util.ArrayList;

import org.springframework.lang.Nullable;

public class InfoPerson {
	String idPerson, name;
	ArrayList<OperationP> arrayOp = new ArrayList<OperationP>();
	
	
	public InfoPerson(String idPerson) {
		super();
		this.idPerson = idPerson;
	}
	
	public int insertOp(String idPerson, int value, String card) {
			OperationP op = new OperationP();
			op.setValue(value);
			op.setCard(card);
			op.setIdOp(arrayOp.size()+1);
			arrayOp.add(op);
			this.setArrayOp(arrayOp);
			getScore();
		return arrayOp.size();
			
	}
	
	public int updateOp(int IdOp, @Nullable int value, @Nullable String card) {
		int answer=0;
		
		int index = findIndex(IdOp);
		if(index < arrayOp.size()) {
			
			OperationP op = arrayOp.get(index);
			if(value != 0)
				op.setValue(value);
			if(!card.isEmpty())
				op.setCard(card);
			
			arrayOp.set(index, op);
			//this.setArrayOp(arrayOp);
			answer=1;
		}
		
	return answer;
		
	}
	
	public int deleteOp(int IdOp) {
		int answer=0;
		
		int index = findIndex(IdOp);
		if(index < arrayOp.size()) {
			arrayOp.remove(index);
			answer=1;
		}
		
	return answer;
		
	}
	
	private int findIndex(int IdOp) {
		int index=arrayOp.size();
		int i=0;
		for (OperationP operation : arrayOp) {
			if(operation.idOp==IdOp)
				index=i;
			i++;
		}
			
		return index;
	}	
	
	public int getScore() {
		int score=0;
		int sum=0;
		if(0 < arrayOp.size()) {
			for (OperationP operation : arrayOp) {
				sum=sum+operation.value;
			}

			if(sum <= 100000)
				score=450;
			else if(sum > 100000 && sum <= 500000)
				score=550;
			else if(sum > 500000 && sum <= 1500000)
				score=640;
			else if(sum > 1500000 && sum <= 3000000)
				score=710;
			else
				score=960;
		}
		return score;
	}	
	
	public String getIdPerson() {
		return idPerson;
	}
	public String getName() {
		return name;
	}
	public ArrayList<OperationP> getArrayOp() {
		return arrayOp;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void setArrayOp(ArrayList<OperationP> arrayOp) {
		this.arrayOp = arrayOp;
	}	
	
	
}
