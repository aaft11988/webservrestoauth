package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;



@SpringBootApplication
@EnableOAuth2Sso
public class RestfulexpApplication extends WebSecurityConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(RestfulexpApplication.class, args);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {		
		http.csrf().disable().antMatcher("/**").authorizeRequests().antMatchers("/", "/login**").permitAll()
				.anyRequest().authenticated().and().logout().deleteCookies("remove").invalidateHttpSession(false)
				.logoutSuccessUrl("/").permitAll().and()
				.formLogin().defaultSuccessUrl("/", true);

	}
	
	
}
/*
@SpringBootApplication
public class RestfulexpApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulexpApplication.class, args);
	}

}*/
