package com.example.demo;



import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ScoreService {
	
	Map<String, InfoPerson> MapPersons = new HashMap<String, InfoPerson>();
	
	@RequestMapping("/user")
	public Principal user(Principal principal) {
		return principal;
	}
	
	@RequestMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.setAuthenticated(false);
        new SecurityContextLogoutHandler().logout(request,response,authentication);
        SecurityContextHolder.clearContext();
        request.logout();
        request.getSession().invalidate();
    }

	//*********** READ
	@RequestMapping(value="/people", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getPeople() {
		String json;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			json = objectMapper.writeValueAsString(MapPersons);
		} catch (Exception e) {
			// TODO: handle exception
			json="{}";
		}
		System.out.println(json);
		return json;
	}	
	@RequestMapping(value="/person", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getPerson(String idPerson) {
		String json;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			InfoPerson persona =MapPersons.get(idPerson);
			json = objectMapper.writeValueAsString(persona);
		} catch (Exception e) {
			// TODO: handle exception
			json="{}";
		}
		System.out.println(json);
		return json;
	}
	
	//*********** CREATE 
	@RequestMapping(value="/CreatePerson", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String createPerson(String idPerson, String name, int value, String card) {
		String json="{}";
		try {
			InfoPerson persona = new InfoPerson(idPerson);
			persona.setName(name);
			persona.insertOp(idPerson, value, card);
			MapPersons.put(idPerson, persona);
			String pattern = "{ \"PersonaCreada\":\"%s\", \"estado\":\"Exitoso\" }";
			json = String.format(pattern, idPerson);
		} catch (Exception e) {
			// TODO: handle exception
			String pattern = "{ \"PersonaCreada\":\"%s\", \"estado\":\"fallo\", \"detalles\":\"%s\" }";
			json = String.format(pattern, idPerson, e);
		}

		System.out.println(json);
		return json;
	}	
	@RequestMapping(value="/CreateOperation", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String createOperation(String idPerson, int value, String card) {
		String pattern, json;
		int size;
		try {
			if(MapPersons.containsKey(idPerson)) {
				InfoPerson persona =MapPersons.get(idPerson);
				size=persona.insertOp(idPerson, value, card);
				MapPersons.replace(idPerson, persona);
				pattern = "{ \"OperacionCreada\":\"%s\", \"estado\":\"Exitoso\" }";
			}else {
				pattern = "{ \"OperacionCreada\":\"%s\", \"estado\":\"fallo\" }";
				size=0;
			}
			json = String.format(pattern, size);
		} catch (Exception e) {
			// TODO: handle exception
			pattern = "{ \"OperacionCreada\":\"%s\", \"estado\":\"fallo\", \"detalles\":\"%s\" }";
			json = String.format(pattern, idPerson, e);
		}
		
		System.out.println(json);
		return json;
	}
	

	//*********** UPDATE 
	@RequestMapping(value="/UpdatePerson", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String updatePerson(String idPerson, @Nullable String name) {
		String json="{}";
		try {
			if(!name.isEmpty() && MapPersons.containsKey(idPerson)) {
				InfoPerson persona =MapPersons.get(idPerson);
				persona.setName(name);
				MapPersons.replace(idPerson, persona);
				String pattern = "{ \"InformaciónActualizada\":\"%s\", \"estado\":\"Exitoso\" }";
				json = String.format(pattern, idPerson);
			}
		} catch (Exception e) {
			// TODO: handle exception
			String pattern = "{ \"InformaciónActualizada\":\"%s\", \"estado\":\"fallo\", \"detalles\":\"%s\" }";
			json = String.format(pattern, idPerson, e);
		}

		System.out.println(json);
		return json;
	}	
	@RequestMapping(value="/UpdateOperation", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String updateOperation(String idPerson, int IdOp, int value, String card) {
		String pattern, json;
		int size;
		try {
			if(MapPersons.containsKey(idPerson)) {
				InfoPerson persona =MapPersons.get(idPerson);
				size=persona.updateOp(IdOp, value, card);
				MapPersons.replace(idPerson, persona);
				pattern = "{ \"OperacionActualizada\":\"%s\", \"estado\":\"Exitoso\" }";
			}else {
				pattern = "{ \"OperacionActualizada\":\"%s\", \"estado\":\"fallo\" }";
				size=0;
			}
			json = String.format(pattern, size);
		} catch (Exception e) {
			// TODO: handle exception
			pattern = "{ \"OperacionActualizada\":\"%s\", \"estado\":\"fallo\", \"detalles\":\"%s\" }";
			json = String.format(pattern, idPerson, e);
		}
		
		System.out.println(json);
		return json;
	}
	
	
	//*********** DELETE 
		@RequestMapping(value="/DeletePerson", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public String deletePerson(String idPerson) {
			String json="{}";
			try {
				if(MapPersons.containsKey(idPerson)) {
					MapPersons.remove(idPerson);
					String pattern = "{ \"PersonaEliminada\":\"%s\", \"estado\":\"Exitoso\" }";
					json = String.format(pattern, idPerson);
				}
			} catch (Exception e) {
				// TODO: handle exception
				String pattern = "{ \"PersonaEliminada\":\"%s\", \"estado\":\"fallo\", \"detalles\":\"%s\" }";
				json = String.format(pattern, idPerson, e);
			}

			System.out.println(json);
			return json;
		}	
		@RequestMapping(value="/DeleteOperation", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public String deleteOperation(String idPerson, int IdOp) {
			String pattern="{}", json;
			int ans=0; 
			try {
				if(MapPersons.containsKey(idPerson)) {
					InfoPerson persona =MapPersons.get(idPerson);
					ans=persona.deleteOp(IdOp);
					if(ans==1) {
						MapPersons.replace(idPerson, persona);
						pattern = "{ \"OperacionEliminada\":\"%s\", \"estado\":\"Exitoso\" }";						
					}
				}
				if(ans!=1)
					pattern = "{ \"OperacionEliminada\":\"%s\", \"estado\":\"fallo\" }";

				json = String.format(pattern, ans);
			} catch (Exception e) {
				// TODO: handle exception
				pattern = "{ \"OperacionEliminada\":\"%s\", \"estado\":\"fallo\", \"detalles\":\"%s\" }";
				json = String.format(pattern, idPerson, e);
			}
			
			System.out.println(json);
			return json;
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	@RequestMapping(value="/score", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String updateScore(int idPerson, int value, String name, String card) {
		this.idPerson = idPerson;
		this.value = value;
		this.name = name;
		this.card = card;
		String pattern = "{ \"idPerson\":\"%s\", \"value\":\"%s\", \"name\":\"%s\", \"card\":\"%s\" }";
		String json = String.format(pattern, idPerson, value, name, card);
		System.out.println(json);
		return json;
	}
	
	
	@RequestMapping(value="/score/wins", method=RequestMethod.POST)
	public int increaseWins() {
		idPerson++;
		return idPerson;
	}
	
	@RequestMapping(value="/score/losses", method=RequestMethod.POST)
	public int increaseLosses() {
		value++;
		return value;
	}
	
	@RequestMapping(value="/score/ties", method=RequestMethod.POST)
	public String increaseTies() {
		name="";
		return name;
	}
	
	@RequestMapping(value="/score/card", method=RequestMethod.POST)
	public String increaseCard() {
		card="";
		return name;
	}
	
	@RequestMapping(value="/score/wins", method=RequestMethod.GET)
	public int getWins() {
		return idPerson;
	}

	@RequestMapping(value="/score/losses", method=RequestMethod.GET)
	public int getLosses() {
		return value;
	}

	@RequestMapping(value="/score/ties", method=RequestMethod.GET)
	public String getTies() {
		return name;
	}
	
	@RequestMapping(value="/score/card", method=RequestMethod.GET)
	public String getCard() {
		return card;
	}
*/

}

